using System;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenDB.Cache.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GardenCommunicationService.Controllers {
    
    [ApiController]
    [Route("[controller]")]
    public class AirPumpController: Controller  {
        
        private readonly ICacheService _pumpTaskService;
        private readonly ILogger<AirPumpController> _logger;
        
        public AirPumpController(ICacheService pumpTaskService, 
            ILogger<AirPumpController> logger) {
            
            _pumpTaskService = pumpTaskService;
            _logger = logger;
        }
        
        [HttpGet]
        public async Task<ActionResult<bool>> GetAirPumpState() {
            try {
                var fanState = await _pumpTaskService.Read(GardenTaskEnum.AirPumpSwitchTask.ScheduleType.StateCheckAction.Value);
                return Ok(fanState);
            }
            catch (Exception e) {
                _logger.LogError(e, "Collection of Air Pump state failed");
                return BadRequest(e.Message);
            }

        }
        
        [HttpPost]
        public async Task<ActionResult<bool>> SwitchAirPump() {
            try {
                var oldState = await _pumpTaskService.Read(GardenTaskEnum.AirPumpSwitchTask.ScheduleType.StateCheckAction.Value);
                var state = await _pumpTaskService.Register(GardenTaskEnum.AirPumpSwitchTask.Name, !oldState);
                return Ok(state);
            }
            catch (Exception e) {
                _logger.LogError(e, "Change of Air Pump state failed");
                return BadRequest(e.Message);
            }
        }
    }
}