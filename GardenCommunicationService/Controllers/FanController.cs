using System;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenDB.Cache.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GardenCommunicationService.Controllers {
    
    [ApiController]
    [Route("[controller]")]
    public class FanController : Controller {
        
        private readonly ICacheService _fanTaskService;
        private readonly ILogger<FanController> _logger;

        public FanController(ILogger<FanController> logger, ICacheService fanTaskService) {
            _fanTaskService = fanTaskService;
            _logger = logger;
        }

        /// <summary>
        /// Get Fan status from in Redis
        /// </summary>
        /// <returns>Fan status as bool (false = off)</returns>
        [HttpGet]
        public async Task<ActionResult<bool>> GetFanState() {
            try {
                var fanState = await _fanTaskService.Read(GardenTaskEnum.FanSwitchTask.ScheduleType.StateCheckAction.Value);
                return Ok(fanState);
            }catch (Exception e) {
                _logger.LogError(e, "Collection of Fan state failed");
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Turn Fan on or off
        /// </summary>
        /// <returns>new fan state</returns>
        [HttpPost]
        public async Task<ActionResult<bool>> SwitchFan() {
            try {
                var oldState = await _fanTaskService.Read(GardenTaskEnum.FanSwitchTask.ScheduleType.StateCheckAction.Value);
                var state = await _fanTaskService.Register(GardenTaskEnum.FanSwitchTask.Name, !oldState);
                return Ok(state);
            }
            catch (Exception e) {
                _logger.LogError(e, "Change of Fan state failed");
                return BadRequest(e.Message);
            }
        }
    }
}