using System;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenDB.Cache.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GardenCommunicationService.Controllers {
    
    [ApiController]
    [Route("[controller]")]
    public class LampController : Controller {
        
        private readonly ICacheService _fanTaskService;
        private readonly ILogger<LampController> _logger;

        public LampController(ICacheService fanTaskService, ILogger<LampController> logger) {
            _fanTaskService = fanTaskService;
            _logger = logger;
        }
        
        [HttpGet]
        public async Task<ActionResult<bool>> GetLampState() {
            try {
                var lampState = await _fanTaskService.Read(GardenTaskEnum.LampSwitchTask.ScheduleType.StateCheckAction.Value);
                return Ok(lampState);
            }
            catch (Exception e) {
                _logger.LogError(e, "Collection of Lamp state failed");
                return BadRequest(e.Message);
            }
            
        }
        
        [HttpPost]
        public async Task<ActionResult<bool>> SwitchLamp() {
            try {
                var oldState = await _fanTaskService.Read(GardenTaskEnum.LampSwitchTask.ScheduleType.StateCheckAction.Value);
                var state = await _fanTaskService.Register(GardenTaskEnum.LampSwitchTask.Name, !oldState);
                return Ok(state);
            }
            catch (Exception e) {
                _logger.LogError(e, "Change of Lamp state failed");
                return BadRequest(e.Message);
            }
            
        }
    }
}