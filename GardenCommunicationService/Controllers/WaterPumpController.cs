using System;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenDB.Cache.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GardenCommunicationService.Controllers {
    
    [ApiController]
    [Route("[controller]")]
    public class WaterPumpController: Controller  {
        
        private readonly ICacheService _pumpTaskService;
        private readonly ILogger<WaterPumpController> _logger;
        
        public WaterPumpController(ICacheService pumpTaskService, 
            ILogger<WaterPumpController> logger) {
            
            _pumpTaskService = pumpTaskService;
            _logger = logger;
        }
        
        [HttpGet]
        public async Task<ActionResult<bool>> GetWaterPumpState() {
            try {
                var fanState = await _pumpTaskService.Read(GardenTaskEnum.WaterPumpSwitchTask.ScheduleType.StateCheckAction.Value);
                return Ok(fanState);
            }
            catch (Exception e) {
                _logger.LogError(e, "Collection of Water Pump state failed");
                return BadRequest(e.Message);
            }

        }
        
        [HttpPost]
        public async Task<ActionResult<bool>> SwitchWaterPump() {
            try {
                var oldState = await _pumpTaskService.Read(GardenTaskEnum.WaterPumpSwitchTask.ScheduleType.StateCheckAction.Value);
                var state = await _pumpTaskService.Register(GardenTaskEnum.WaterPumpSwitchTask.Name, !oldState);
                return Ok(state);
            }
            catch (Exception e) {
                _logger.LogError(e, "Change of Water Pump state failed");
                return BadRequest(e.Message);
            }
        }
    }
}